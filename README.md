# Polysphères

Interface de vote pour les Polysphères

## Mise à jour annuelle

- La liste des candidats est dans data.json
- Facile à mettre à jour en utilisant un outil comme https://excel2json.io (NB: 100% client-side) pour produire le bon format

Server-side :
- backup la db, exporter sur le drive
- `DELETE FROM votes;` (l'autre table n'est plus utilisée)