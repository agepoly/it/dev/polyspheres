const express = require('express')
const path = require('path')
const http = require('http')
const dotenv = require('dotenv')

const app = express()
const envLoadResult = dotenv.config()

const tequila = require('./tequila')
const db = new (require('./db'))()
console.log('Loaded DB')

const teacherData = require('./data.json').map(entry => {
  entry.name = entry.firstname + ' ' + entry.name
  delete entry.firstname
  return entry
})

if (process.env.NODE_ENV !== 'production' && !envLoadResult.error) {
  console.log(envLoadResult.parsed)
}

if (process.env.USE_PROXY) {
  app.set('trust proxy', true)
}

function UnitToFaculty (str) {
  if (!str) return '??'
  const validFaculties = ['in', 'sc', 'cgc', 'ma', 'ph', 'gm', 'mx', 'el', 'mt', 'ar', 'gc', 'sie', 'sv', 'mte', 'if', 'shs', 'dh']
  for (const i of str.split(',')) {
    for (const f of validFaculties) {
      if (i.indexOf(f + '-') === 0) {
        return f
      }
    }
  }
  return '??'
}

console.log('Application starting...')
const httpServer = http.createServer(app)

app.use((req, res, next) => {
  req.ip =
        (req.headers['x-forwarded-for'] || '').split(',').pop().trim() ||
        req.connection.remoteAddress ||
        req.socket.remoteAddress ||
        req.connection.socket.remoteAddress
  next()
})

app.use((req, res, next) => {
  req.getUrl = req.protocol + '://' + req.get('host') + req.originalUrl
  return next()
})

app.use(require('cookie-parser')())

app.get('/home', tequila, (req, res) => {
  res.sendFile(path.join(__dirname, '/static/index.html'))
})
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '/static/login.html'))
})

app.get('/vote', tequila, async (req, res, next) => {
  const id = req.tequila.sciper
  const sc = UnitToFaculty(req.tequila.units)

  const m = [req.query.m1, req.query.m2, req.query.m3]
  const p = [req.query.p1, req.query.p2, req.query.p3]

  // valid
  if (!p[0] || !p[1] || !p[2]) {
    res.status(400)
    return res.json({ error: 'Invalid Data', messsage: "Vous n'avez pas voté pour 3 candidats." })
  }
  // duplicated
  if (p[0] === p[1] || p[1] === p[2] || p[0] === p[2]) {
    res.status(400)
    return res.json({ error: 'Invalid Data', messsage: 'Vous ne pouvez pas voter deux fois pour le même professeur.' })
  }
  const valid = teacherData.filter(v => (v.id == p[0] || v.id == p[1] || v.id == p[2]))
  console.log(valid, p)
  if (valid.length !== 3) {
    res.status(400)
    return res.json({ error: 'Invalid Data', messsage: 'Vous avez voté pour des candidats inexistants.' })
  }

  db.submitVote(id, sc, p, m).then(() => {
    res.status(200)
      .cookie('voted', true)
      .send({ message: 'Merci pour votre vote, celui-ci a été enregistré' })
  }).catch(err => {
    if (err === 'Duplicate Vote') {
      res.status(500).cookie('voted', true)
    }
    next(err)
  })
})

app.get('/candidates', tequila, (req, res) => {
  res.send(teacherData)
})

app.use(express.static('static'))

// Catch 404 and forward to error handler
app.use((req, res, next) => {
  res.status(404)
  res.json({
    error: 404,
    status_code: 404,
    status: 'Error - Not Found',
    message: 'Url not found : ' + req.originalUrl
  })
})

// Error Handler Middleware
app.use((err, req, res, next) => {
  console.log(req.connection.remoteAddress)
  console.error(err)

  res.status(500)
  res.json({ error: '500', messsage: String(err) })
})

const serverPort = process.env.PORT || 3000
httpServer.listen(serverPort, () =>
  console.info('HTTP Server Listening on port', serverPort)
)

module.exports = app
