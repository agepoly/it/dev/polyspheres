/* eslint-disable no-multi-str */
const { Pool } = require('pg')

const DBConn = class DBConn {
  constructor () {
    if (process.env.NODE_ENV !== 'production') {
      this.dev = true
      require('sqlite-async').open('test.db')
        .then(db => {
          this.testdb = db
          this.testdb.exec(
            `CREATE TABLE votes (
                         sciper INT PRIMARY KEY, faculty VARCHAR(3) NOT NULL,
                         p1 INT, p2 INT, p3 INT,
                         m1 VARCHAR(512), m2 VARCHAR(512), m3 VARCHAR(512)
                     )`).catch(() => {
            // we ignore since it means table is created
          })
        })
      return
    }
    this.pool = new Pool({
      user: process.env.DB_USER,
      host: process.env.DB_HOST,
      database: process.env.DB_NAME,
      password: process.env.DB_PASSWORD,
      port: process.env.DB_PORT
    })
    this.pool.on('error', (err, client) => {
      console.error('Unexpected error on idle client', err)
    })
  }

  submitVote (id, sc, p, m) {
    if (this.dev) {
      return this.testdb.run('INSERT INTO votes (sciper, faculty, p1, p2, p3, m1, m2, m3) \
                            VALUES ($1, $2, $3, $4, $5, $6, $7, $8)', [id, sc,
        p[0], p[1], p[2], m[0], m[1], m[2]])
        .catch(err => {
          if (err && err.errno === 19) {
            throw 'Duplicate Vote'
          } else {
            console.error('DB Error : ', err)
            throw 'Internal Error'
          }
        })
    }

    return this.pool.query('INSERT INTO votes (sciper, faculty, p1, p2, p3, m1, m2, m3) \
                            VALUES ($1, $2, $3, $4, $5, $6, $7, $8)', [id, sc,
      p[0], p[1], p[2], m[0], m[1], m[2]])
      .catch(err => {
        console.log(err)
        if (err && err.code === '23505') {
          throw 'Duplicate Vote'
        } else {
          console.error('DB Error : ', err)
          throw 'Internal Error'
        }
      })
  }
}

module.exports = DBConn
